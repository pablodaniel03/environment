#!/bin/bash
#####################################
# Pablo Almaguer
# Shell Profile for VM
#
# Ver: 1.0 [20192910]
#
SCRIPT_NAME="$(basename "${BASH_SOURCE[0]}")"
SCRIPT_PATH="$(dirname "${BASH_SOURCE[0]}")"

# Linux Settings
source /etc/os-release

local_profile="${HOME}/.bash_profile"

if [ -f "${HOME}/.profile" ]; then
   local_profile="${HOME}/.profile" 
fi

export HOSTNAME="$(uname -n)"
export PROCESSORS="$(lscpu | sed -ne '4s/^[^ ]* *//p')"          # Numero de Procesadores
export CPU_MHz="$(lscpu | grep "CPU MHz" | awk '{print $3}')"    # Velocidad de Procesadores
export RAM_MEMORY="$(awk '$3=="kB"{$2=$2/1024**2;$3="GB";} 1' /proc/meminfo | column -t | grep MemTotal | awk '{print $2,$3}')" # Memoria RAM
#export OS_VERSION="$(cat /etc/system-release)" 				     # Redhat/Fedora
export OS_VERSION="${NAME} ${VERSION}"

export TMP="/tmp"
export TEMP="${TMP}"
export TMPDIR="${TMP}"
export TEMPDIR="${TMP}"
export STAGE_HOME="${HOME}/stage"
export SCRIPTS="${HOME}/scripts"
export PATH="/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:${HOME}/bin:${LOGIC}"

alias nawk='awk'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias ff='f=`ls -td */ | head -1`; cd $f; ls -ltr'
alias tlf='f="$(find . -type f -exec stat --format '\''%Y%y%n'\'' '\''{}'\'' \; | sort -nr | awk -F / '\''NR==1{print $NF}'\'')"; tail -300f $f'
alias ls='ls --color=none'
alias ltr='ls -ltr'
alias ll='ls -l'

_timestamp() { echo "$(date +"%m%d%Y%H%M%S")"; }
     _date() { echo "$(date +"%m%d%Y")"; }
   profile() { arg="${1}"; . ${local_profile} ${arg}; }

# Java home
export JAVA_HOME="/u01/Java/jdk1.7.0_80"
export JRE_HOME="${JAVA_HOME}/jre"
export JAVA_VERSION="$(java -version 2>&1 | awk '/Runtime/')"
export JAVA_ARGS="-Xmx1024m -XX:MaxPermSize=1024m -XX:-UseGCOverheadLimit -Djava.security.egd=file:/dev/./urandom"

export PATH="${PATH}:${JAVA_HOME}/bin"

echo " "
echo "Windows Subsystem Linux environment..."
