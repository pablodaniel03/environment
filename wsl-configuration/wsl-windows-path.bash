
DEV_BINARIES="/mnt/d/Development/binaries"
DEV_WORKSPACES="/mnt/d/Development/workspaces"

WIN_D_X64_PROGRAMS="/mnt/d/Program\ Files"
WIN_C_X64_PROGRAMS="/mnt/c/Program\ Files"
WIN_C_X86_PROGRAMS="/mnt/c/Program\ Files\ \(x86\)"
WIN_SYSTEM32="/mnt/c/Windows/System32"


windows_cmd="${WIN_SYSTEM32}/cmd.exe"
windows_psh="${WIN_SYSTEM32}/WindowsPowerShell/v1.0/powershell.exe"
vs_code="${WIN_D_X64_PROGRAMS}/Microsoft\ VS\ Code/Code.exe"

alias cmd='$windows_cmd'
alias powershell='$windows_psh'

alias code='$vs_code'

# Python 3.8
python_home="${DEV_BINARIES}/python-3.8.3"
PATH="${PATH}:${python_home}"

export PATH

echo "Windows Environment in \"${OS_VERSION}\""

